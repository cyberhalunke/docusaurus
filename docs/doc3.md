---
id: doc3
title: This is document number 3
---
# Streaming

* [x] [Donald Trumps beste Wahlkampfrede ](https://invidious.snopyta.org/watch?v=RyBVAbzAps8)


* **[Burning Series](https://bs.to)**: [Alternativ](https://burningseries.co/)
* **[Media CCC](https://media.ccc.de/)**
* **[Netflix](https://www.netflix.com/)**
* **[Plex](https://app.plex.tv/desktop)**
* **[Archiv.org](https://archive.org/details/@danimedia)**

## Filehoster

* <https://www.premiumize.me/>

## Salt

* **[Dashboard](https://login.salt.ch/cas-external/login?service=https%3a%2f%2ffiber.salt.ch%2fmyaccount%2f%3flang%3dde#/TVParameters)**
* **[Kontakt](https://fiber.salt.ch/de/contact)**


* <https://tv.salt.ch/login>


* <https://video.salt.ch/de/login>

## Video Plattform

* <https://vivo.sx/>
* <https://jetload.net/#>!/
* <https://vimeo.com/de/>
* <https://www.bitchute.com/>
* <https://lbry.tv/>
* <https://odysee.com/>
* <https://www.youtube.com/>
* <https://invidio.us/>

## Audio

* <https://hearthis.at/punisher/>
* <https://clyp.it/>
* <https://www.spotify.com/de/>

## Image

* <https://www.directupload.net/>

## Dokumente

* <https://de.scribd.com/upload-document>


This is a link to [another document.](doc3.md)  
This is a link to an [external page.](http://www.example.com)
